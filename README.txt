(C) TheChiefMeat 2016-2019. All rights reserved.

KeePass Launcher is a tool designed to automatically launch your KeePass database
without any user intervention, find information about added features in the below
patch notes.

Download link:

https://bitbucket.org/TheChiefMeat/keepass-launcher/downloads/keepass-launcher-latest.zip

Requirements:

KeePass database (.kdbx) file association already setup to use KeePass/KeePassXC/KeeWeb.

Installation:

Extract the .zip file to a location of your choice.
Run the KeePass Launcher.exe
Follow the displayed instructions regarding configuration and setup.

Features:

Support for password only, password & keyfile and keyfile only modes.
Credential encryption (now using AES-256)
Support for KeePass installation & portable KeePass.
Customisable locations for all relevant files (see generated config.ini at startup).

For further support email me at TheChiefMeat@gmail.com

To donate please go to https://www.paypal.me/TheChiefMeat/5