Func FileLocation()
	;FORM (GUI) CREATION - Creates the basic interface for the starup messages and interface (each number represents a variable,
	; (X-axis, Y-axis, Length, Width, and possible others)
	FileOpen(@ScriptDir & "\config.ini", 1)
	$GUI = _Metro_CreateGUI("KeePass Launcher" & $VERSION_Launcher, 166, 350, -1, -1, True)
	$EXE_Button = _Metro_CreateButton("Executable", 18, 20, 130, 40)
	$KDBX_Button = _Metro_CreateButton("Database", 18, 65, 130, 40)
	$KEYFILE_Button = _Metro_CreateButton("Keyfile", 18, 110, 130, 40)
	$DONE = _Metro_CreateButton("Done", 18, 155, 130, 40)
	$Exit = _Metro_CreateButton("Exit", 18, 245, 130, 40)
	$GUIDE = _Metro_CreateButton("Guide", 18, 200, 130, 40)
	;Detects if a previously created .xml security file has been made, if detected it is deleted and then the Startup Function is run.
	If FileExists("*.xml") Then
	$XMLDETECTED = _Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "FIRST TIME USE: Previous use found! Cleaning up folder...", 410, 11, "", 3)
	FileDelete("*.xml")
	Endif
	$DonationMSG = _Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "If you find this program useful, please use the donation button found during startup, or by using the link found in the README.txt file." & @CRLF & @CRLF & "Please ensure that your database is associated with KeePass/KeePassXC/KeeWeb before continuing." & @CRLF & @CRLF & "To see the latest changes, please refer to the included patch notes.", 380, 11)
	;Checks to see if the Licence agreement has been agreed to and shows the licence agreement (VERY LONG LINE OF CODE, USE WRAP TO SEE!!)
	$LICENCE = _Metro_MsgBox(1, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "(C) TheChiefMeat 2016-2018. All rights reserved." & @CRLF & @CRLF & "DISCLAIMER/LICENCE" & @CRLF & @CRLF & "WHILE I HAVE MADE EVERY POSSIBLE EFFORT TO ENSURE THIS SOFTWARE IS BUG FREE," & @CRLF & "I DO NOT GUARANTEE THAT IT IS FREE FROM DEFECTS. THIS SCRIPT IS PROVIDED AS IS" & @CRLF & "AND YOU USE THIS SCRIPT AT YOUR OWN RISK. UNDER NO CIRCUMSTANCE SHALL I BE HELD" & @CRLF & "LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES, LOSSES OR THEFT FROM USE OR MISUSE OF THIS SOFTWARE." & @CRLF & @CRLF & "FOR PRIVATE USE YOU MAY:" & @CRLF & @CRLF & "MODIFY THIS SOFTWARE ANY WAY YOU SEE FIT, HOWEVER SAID MODIFIED VERSIONS ARE NOT TO BE REDISTRIBUTED VIA ANY MEANS." & @CRLF & @CRLF & "FOR PRIVATE USE YOU MAY NOT:" & @CRLF & "REDISTRIBUTE THIS SOFTWARE." & @CRLF & "REDISTRIBUTE MODIFIED VERSIONS OF THIS SOFTWARE." & @CRLF & "COMMERCIALISE THIS SOFTWARE." & @CRLF & "SELL THIS SOFTWARE." & @CRLF & @CRLF & "ANY COMMERCIAL USE OF THIS SOFTWARE IS STRICTLY PROHIBITED." & @CRLF & "ANY COMMERCIALISATION OF THIS SOFTWARE IS STRICTLY PROHIBITED." & @CRLF & @CRLF & "ALL DOWNLOAD LINKS MUST POINT DIRECTLY TO THE BELOW LINK:" & @CRLF & @CRLF & "OFFICIAL BITBUCKET REPOSITORY:" & @CRLF & @CRLF & "https://bitbucket.org/TheChiefMeat/keepass-launcher/downloads/keepass-launcher-latest.zip", 650, 11)
	If $LICENCE = "Cancel" Then
		FileDelete("config.ini")
		EXIT 0
	Elseif $LICENCE = "OK" Then
	EndIf
	;Calls the MODE function from the MiscFunctions.au3 file.
	MODE()

	;Allows the user to add KeePass Launcher to Windows Startup
	$StartupBoot = _Metro_MsgBox(4, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "Run KeePass Launcher and login on boot?", 310, 11)

	If $StartupBoot = "YES" Then
		FileCreateShortcut(@ScriptDir & "\KeePass Launcher.exe", @AppDataDir & "\Microsoft\Windows\Start Menu\Programs\Startup\KeePass Launcher.lnk", @ScriptDir, "", "KeePass Launcher", @ScriptDir & "\KeePass Launcher.exe", "", "")
	ElseIf $StartupBoot = "NO" Then
		If FileExists (@AppDataDir & "\Microsoft\Windows\Start Menu\Programs\Startup\KeePass Launcher.lnk") Then
			FileDelete (@AppDataDir & "\Microsoft\Windows\Start Menu\Programs\Startup\KeePass Launcher.lnk")
		EndIf
	EndIf

	$STARTUPMSG = _Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "FIRST TIME USE: Please select your KeePass Manager, KeePass Database and Keyfile...", 400, 11)
	;Here we are changing the button colour variable to a green colour and putting the Donate button under that variable, causing
	;only that button to be green.
	$ButtonBKColor = "0x40C338"
	$Donate = _Metro_CreateButton("Donate", 18, 290, 130, 40)
	;Fixes the GUI positioning bug.
	GUICtrlSetResizing($EXE_Button, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($KDBX_Button, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($KEYFILE_Button, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($DONE, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($GUIDE, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($Exit, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($Donate, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	; Display the GUI.z
	GUISetState(@SW_SHOW)
	; Loop until the user exits. (This is required to keep the interface on the screen).
	While 1
		_Metro_HoverCheck_Loop($GUI)
		Switch GUIGetMsg()
			;Cases are "Events"; Things that happen on the screen. Here you can see the donation message occur, then the startup msg, etc.
			;Usually you want each "event" to be in their own Case.
			Case $GUI_EVENT_CLOSE
				ExitLoop
			Case $DonationMSG
				$DonationMSG
			Case $STARTUPMSG
				$STARTUPMSG
			Case $EXE_Button
				;Here we ask the user to locate the files so we can fill in the variables ($EXE_Location, $KDBX_Button, $KEYFILE_Location).
				$EXE_Location = FileOpenDialog("Please select your KeePass executable (KeePass.exe/KeePassXC.exe/KeeWeb.exe)", @DesktopDir, "KeePass executable (*.exe)", 1)
			Case $KDBX_Button
				$KDBX_Location = FileOpenDialog("Please select your KeePass Database", @DesktopDir, "KeePass Database (*.kdbx)", 1)
			Case $KEYFILE_Button
				$KEYFILE_Location = FileOpenDialog("Please select your Keyfile", @DesktopDir, "KeePass Keyfile (*.key)", 1)
			Case $DONE
				;Once we're done with the Form we can delete it from the memory.
				GUIDelete($GUI)
				;CREATE ARRAY FROM THE .EXE LOCATION, PAIR NAME AND EXTENSION TOGETHER TO FORM $VERSION_KEEPASS VARIABLE
				;This is required so we can send the "KeePass.exe" infomation to the $VERSION_KEEPASS variable correctly without
				;the extension missing.
				Local $sDrive = "", $sDir = "", $sFileName = "", $sExtension = ""
				Local $aPathSplit = _PathSplit($EXE_Location, $sDrive, $sDir, $sFileName, $sExtension)
				Local $VERSION_KEEPASS = $sFileName & $sExtension
				;Here we are writing the variables and other information to the config.ini file so we can use it later to log in with.
				IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "ApplicationInstallLocation", $EXE_Location)
				IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "DatabaseLocation", $KDBX_Location)
				IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "PortableMode", "NO")
				If $VERSION_KEEPASS = "KeePass.exe" Then
					IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "PauseTime", "750")
				Elseif $VERSION_KEEPASS = "KeePassXC.exe" Then
					IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "PauseTime", "1000")
				Elseif $VERSION_KEEPASS = "KeeWeb.exe" Then
					IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "PauseTime", "1500")
				EndIf
				IniWrite(@ScriptDir & "\config.ini", "KeyfileSettings", "KEYFILELocation", $KEYFILE_Location)
				IniWrite(@ScriptDir & "\config.ini", "EncryptionFileSettings", "KeePassUsrDataLocation", "KeePassUsrData.xml")
				IniWrite(@ScriptDir & "\config.ini", "Startup", "VERSION", $VERSION_KEEPASS)
				IniWrite(@ScriptDir & "\config.ini", "Startup", "Startup", "1")
				;Here we are changing the colour button variable back to purple and presenting a restart message, then the program exits.
				$ButtonBKColor = "0x512DA8"
				$STARTUPMSG = _Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "    FIRST TIME USE: Please restart KeePass Launcher...", 400, 11)
				Exit 0
			Case $GUIDE
				$ButtonBKColor = "0x512DA8"
				$GUIDEMSG = _Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "KeePass Launcher is a tool designed to automatically launch your KeePass database without any user intervention." & @CRLF & @CRLF & "Use the 'Executable', 'Database' and 'Keyfile' buttons on the main interface to supply to the location of the relevant files, and then click 'Done'." & @CRLF & @CRLF & "Once KeePass Launcher has been restarted it will ask for your database master password, which can be provided in the entry window." & @CRLF & @CRLF & "Once this is complete, simply restart KeePass Launcher to launch your database.", 320, 11)
			;This case shows the Exit button on the main interface.
			Case $Exit
				GUIDelete($GUI)
				FileDelete("config.ini")
				Exit 0
			;This case shows the Donate button on the main interface and links it directly to PayPal.
			Case $Donate
				ShellExecute("https://www.paypal.me/TheChiefMeat/5")
		EndSwitch
	WEnd
	; Delete the previous GUI and all controls.
	GUIDelete($GUI)
EndFunc   ;==>FileLocation
