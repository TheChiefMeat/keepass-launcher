Func Encrypt()
	$STARTUPMSG = _Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "FIRST TIME USE: Please enter your database MASTER PASSWORD!...", 490, 11)
	;Checks to see if Portable mode is turned on or off.
	If $PortableMode = "NO" Then

	; GET PC INFO (Processor, Windows Serial, Windows Install Date & Time. This information will be used to "hash" the password
	;that is given by the user to create a unique key for that particular install and PC; No other PC will be able to read this
	;key, nor will any other installation of Windows as it is directly tied to the PC.

	Dim $strComputer, $objWMIService
	Const $wbemFlagReturnImmediately = 0x10
	Const $wbemFlagForwardOnly = 0x20
	$objWMIService = ObjGet("winmgmts:{(RemoteShutdown)}//" & "localhost" & "\root\CIMV2")

	Local $colItems = ""
	$colItems = $objWMIService.ExecQuery("Select * from Win32_Processor")
	For $objItem In $colItems
		Local $Processor = $objItem.Name
	Next
	Local $colItems = ""
	$colItems = $objWMIService.ExecQuery("Select * from Win32_OperatingSystem")
	For $objItem In $colItems
		Local $Serial = $objItem.SerialNumber
	Next
	Local $colItems = ""
	$colItems = $objWMIService.ExecQuery("Select * from Win32_OperatingSystem")
	For $objItem In $colItems
		Local $date = $objItem.InstallDate
		$InstallDate = (StringMid($date, 5, 2) & "/" & _
				StringMid($date, 7, 2) & "/" & StringLeft($date, 4) _
				 & " " & StringMid($date, 9, 2) & ":" & _
				StringMid($date, 11, 2) & ":" & StringMid($date, _
				13, 2))
	Next

	;Combines the three salts into one variable
	$Salts = $Processor & $Serial & $InstallDate

	;If portable mode is turned on, uses the USB serial instead of the PC data.
	ElseIf $PortableMode = "YES" Then
	$Salts = DriveGetSerial("\")
	EndIf

	; Here we are creating the password input GUI.
	$GUI = _Metro_CreateGUI("KeePass Launcher " & @CRLF & $VERSION_Launcher, 350, 120, -1, -1, True)
	$sPasswd = GUICtrlCreateInput("", 35, 25, 280, 20, $ES_PASSWORD)
	$EXE_Button = _Metro_CreateButton("OK", 110, 63, 130, 40)
	GUICtrlSetResizing($EXE_Button, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($sPasswd, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUISetState(@SW_SHOW)
	While 1
		_Metro_HoverCheck_Loop($GUI)
		Switch GUIGetMsg()
			Case $GUI_EVENT_CLOSE
			Case $EXE_Button
				;Here we are reading the information provided in the textinput box and storing it in a variable.
				$sPasswd = GUICtrlRead($sPasswd)
				;Now we use the salting information provided before and combine that with the $sPasswd variable to make a key
				;and write that to the KeePaassUsrData.xml file.
				Local $sEncrypted = StringEncrypt(True, $sPasswd, $Salts)
				FileOpen(@ScriptDir & "\KeePassUsrData.xml", 1)
				FileWrite("KeePassUsrData.xml", $sEncrypted)
				GUIDelete($GUI)
				;A setup complete box is then shown, and the user is asked to restart the application.
				_Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "SETUP COMPLETE!: Please restart KeePass Launcher...", 400, 11)
				ExitLoop
		EndSwitch
	WEnd
EndFunc   ;==>Encrypt
