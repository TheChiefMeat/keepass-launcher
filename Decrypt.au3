;DECRYPTION OF XML MASTER KEY
Func Decrypt()
	;Looking for KeePass.exe, if not running then exit, otherwise continue.
	If ProcessExists($VERSION_KEEPASS) Then
		Running()
		Exit 0
	EndIf
	;Checks to see if Portable mode is turned on or off.
	If $PortableMode = "NO" Then
		; GET PC INFO (Processor, Windows Serial, Windows Install Date & Time. This information will be used to "hash" the password
		;that is given by the user to create a unique key for that particular install and PC; No other PC will be able to read this
		;key, nor will any other installation of Windows as it is directly tied to the PC.
		Dim $strComputer, $objWMIService
		Const $wbemFlagReturnImmediately = 0x10
		Const $wbemFlagForwardOnly = 0x20
		$objWMIService = ObjGet("winmgmts:{(RemoteShutdown)}//" & "localhost" & "\root\CIMV2")

		Local $colItems = ""
		$colItems = $objWMIService.ExecQuery("Select * from Win32_Processor")
		For $objItem In $colItems
			Local $Processor = $objItem.Name
		Next
		Local $colItems = ""
		$colItems = $objWMIService.ExecQuery("Select * from Win32_OperatingSystem")
		For $objItem In $colItems
			Local $Serial = $objItem.SerialNumber
		Next
		Local $colItems = ""
		$colItems = $objWMIService.ExecQuery("Select * from Win32_OperatingSystem")
		For $objItem In $colItems
			Local $date = $objItem.InstallDate
			$InstallDate = (StringMid($date, 5, 2) & "/" & _
					StringMid($date, 7, 2) & "/" & StringLeft($date, 4) _
					 & " " & StringMid($date, 9, 2) & ":" & _
					StringMid($date, 11, 2) & ":" & StringMid($date, _
					13, 2))
		Next
		;Combines the three salts into one variable
		$Salts = $Processor & $Serial & $InstallDate
		;If portable mode is turned on, uses the USB serial instead of the PC data.
	ElseIf $PortableMode = "YES" Then
		$Salts = DriveGetSerial("\")
	EndIf

	If $VERSION_KEEPASS = "KeePass.exe" Then
		;Looking to see if the StartMinimised variable in the config.ini is flagged as NO, if set to NO then
		;the application proceeds as normal, if set to YES then starts in a minimised fashion.

		;Looking to see if Keyfile value is more than null, if more than null, assumes keyfile exists.
		;Once this is done the Decrypt function then retrieves the Processor, Windows Serial, Windows Install Date & Time
		;infomation and decrypts the password (Line 270) to see if the values match.

		;If the values match, then the password will be able to be decrypted, if not then the decryption will fail.
		;This process happens each time the application is run, and you can see this happening below.
		If $KEYFILE_Location = "" Then
			$working = FileRead($MASTER_KEY_Location)
			; Decrypt the encrypted password using the Processor, Windows Serial, Windows Install Date & Time information
			Local $sDecrypted = StringEncrypt(False, $working, $Salts)
			;RUN KEEPASS VERSION AND GUI NAVIGATION
			;The ShellExecute function requires the parameters to be encapsulated like "so".
			;To do this we """" and add a & to ensure the parameters are correctly spaced out.
			;(This was actually a bitch to figure out, some languages hate spacing in paths, argh!!!! :S
			ShellExecute($EXE_Location, """" & $KDBX_Location & """" & " " & "-pw:" & $sDecrypted)
			Exit
			;If keyfile found, continues.
		ElseIf FileExists($KEYFILE_Location) Then
			$working = FileRead($MASTER_KEY_Location)
			; Decrypt the encrypted password using the Processor, Windows Serial, Windows Install Date & Time information
			Local $sDecrypted = StringEncrypt(False, $working, $Salts)
			;Checks to see if password has a stored value, if the value equals nill then the password file is ignored.
			If $sDecrypted = "" Then
				ShellExecute($EXE_Location, """" & $KDBX_Location & """" & " " & "-keyfile:" & """" & $KEYFILE_Location & """")
			Else
				;RUN KEEPASS VERSION AND GUI NAVIGATION
				ShellExecute($EXE_Location, """" & $KDBX_Location & """" & " " & "-pw:" & $sDecrypted & " " & "-keyfile:" & """" & $KEYFILE_Location & """")
			EndIf
			Exit
		Else
			MissingKeyfile()
			Exit
		EndIf
	ElseIf $VERSION_KEEPASS = "KeePassXC.exe" Then
		If $KEYFILE_Location = "" Then
			; Forces the Config.ini to set minimised to no as not supported yet.
			IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "StartMinimised", "NO")
			$working = FileRead($MASTER_KEY_Location)
			; Decrypt the encrypted password using the Processor, Windows Serial, Windows Install Date & Time information
			Local $sDecrypted = StringEncrypt(False, $working, $Salts)
			; Starts a small window informing the user that the databse is about to be launched, window is forced to front
			$StartGUI = _Metro_CreateGUI("", 240, 80, -1, -1, True)
			$Launch = _Metro_CreateButton("Launching Database...", 20, 20, 200, 40)
			GUICtrlSetResizing($Launch, $GUI_DOCKALL + $GUI_DOCKBORDERS)
			GUISetState(@SW_SHOW)
			WinSetOnTop($StartGUI, "", $WINDOWS_ONTOP)
			; Executes the database and waits for the process to be active
			Local $iPID = ShellExecute($KDBX_Location, "")
			ProcessWait($VERSION_KEEPASS)
			Sleep($PauseTime)
			;Splits the drive path into its relavant pieces to find the database name
			Local $sDrive = "", $sDir = "", $sFileName = "", $sExtension = ""
			Local $aPathSplit = _PathSplit($KDBX_Location, $sDrive, $sDir, $sFileName, $sExtension)
			Local $DATABASE = $sFileName & $sExtension
			; Waits for active KeePassXC window with database name, then sends keystroke information to that window
			WinWait($DATABASE & " - KeePassXC")
			Send($sDecrypted)
			Send("{TAB 3}")
			Send("{TAB 3}")
			Send("{ENTER}")
		ElseIf FileExists($KEYFILE_Location) Then
			; Forces the Config.ini to set minimised to no as not supported yet.
			IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "StartMinimised", "NO")
			$working = FileRead($MASTER_KEY_Location)
			; Decrypt the encrypted password using the Processor, Windows Serial, Windows Install Date & Time information
			Local $sDecrypted = StringEncrypt(False, $working, $Salts)

			$StartGUI = _Metro_CreateGUI("", 240, 80, -1, -1, True)
			$Launch = _Metro_CreateButton("Launching Database...", 20, 20, 200, 40)
			GUICtrlSetResizing($Launch, $GUI_DOCKALL + $GUI_DOCKBORDERS)
			GUISetState(@SW_SHOW)
			WinSetOnTop($StartGUI, "", $WINDOWS_ONTOP)

			Local $iPID = ShellExecute($KDBX_Location, "")
			ProcessWait($VERSION_KEEPASS)
			Sleep($PauseTime)

			Local $sDrive = "", $sDir = "", $sFileName = "", $sExtension = ""
			Local $aPathSplit = _PathSplit($KDBX_Location, $sDrive, $sDir, $sFileName, $sExtension)
			Local $DATABASE = $sFileName & $sExtension

			WinWait($DATABASE & " - KeePassXC")
			Send($sDecrypted)
			Send("{TAB 3}")
			Send($KEYFILE_Location)
			Send("{TAB 3}")
			Send("{ENTER}")
		Else
			MissingKeyfile()
			Exit
		EndIf
	ElseIf $VERSION_KEEPASS = "KeeWeb.exe" Then
		; Forces the Config.ini to set minimised to no as not supported yet.
		IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "StartMinimised", "NO")
		; Decrypt the encrypted password using the Processor, Windows Serial, Windows Install Date & Time information
		$working = FileRead($MASTER_KEY_Location)
		Local $sDecrypted = StringEncrypt(False, $working, $Salts)

		$StartGUI = _Metro_CreateGUI("", 240, 80, -1, -1, True)
		$Launch = _Metro_CreateButton("Launching Database...", 20, 20, 200, 40)
		GUICtrlSetResizing($Launch, $GUI_DOCKALL + $GUI_DOCKBORDERS)
		GUISetState(@SW_SHOW)
		WinSetOnTop($StartGUI, "", $WINDOWS_ONTOP)

		Local $iPID = ShellExecute($KDBX_Location, "")
		ProcessWait($VERSION_KEEPASS)
		Sleep($PauseTime)
		WinWait("KeeWeb")
		Send($sDecrypted)
		Send("{ENTER}")
	EndIf
EndFunc   ;==>Decrypt
