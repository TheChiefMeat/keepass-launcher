Func MissingKeyfile() ;Simple Form function to let the user know if the security file (keyfile) cannot be found.
	$STARTUPMSG = _Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "Cannot find keyfile! Please insert USB security stick...", 370, 11)
	WinSetOnTop($STARTUPMSG, "", $WINDOWS_ONTOP)
	Exit
EndFunc   ;==>MissingKeyfile

Func Running() ;Simple "Already running" window to inform you if KeePass/KeeWeb/KeePassXC is already running.
	Local $sDrive = "", $sDir = "", $sFileName = "", $sExtension = ""
	Local $aPathSplit = _PathSplit($EXE_Location, $sDrive, $sDir, $sFileName, $sExtension)
	;Detects what version of KeePass is running, and then scales the window to fit the text.
	If $sFileName = "KeePass" Then
		$Size = "320"
	Elseif $sFileName = "KeePassXC" Then
		$Size = "360"
	Elseif $sFileName = "KeeWeb" Then
		$Size = "320"
	EndIf

	$STARTUPMSG = _Metro_MsgBox(4, "KeePass Launcher " & @CRLF & $VERSION_Launcher, $sFileName & " is already running, restart " & $sFileName & "?", $Size, 11)
	WinSetOnTop($STARTUPMSG, "", $WINDOWS_ONTOP)

	If $STARTUPMSG = "YES" Then
		ProcessClose($sFileName & ".exe")
		sleep(100)
		Decrypt()
		Exit
	Elseif $STARTUPMSG = "NO" Then
		Exit
	EndIf
EndFunc

Func MODE() ;Allows the user to select either fixed or portable mode.
	_Metro_MsgBox(0, "KeePass Launcher " & @CRLF & $VERSION_Launcher, "FIRST TIME USE: Please select either fixed or portable mode!..." & @CRLF & @CRLF & "Fixed Mode: Ties the security file to your PC (Recommended)." & @CRLF & @CRLF & "Portable mode: Ties the security file to the current drive. DO NOT put all associated files in one place as Portable mode is much less secure this way!", 440, 11)
	$PortableGUI = _Metro_CreateGUI("Example", 315, 80, -1, -1, True)
	$FixedButton = _Metro_CreateButton("Fixed", 20, 20, 130, 40)
    $PortableButton = _Metro_CreateButton("Portable", 165, 20, 130, 40)
	GUICtrlSetResizing($FixedButton, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUICtrlSetResizing($PortableButton, $GUI_DOCKALL + $GUI_DOCKBORDERS)
	GUISetState(@SW_SHOW)
	While 1
		_Metro_HoverCheck_Loop($PortableGUI) ;Add hover check in loop
		Switch GUIGetMsg()
			Case $GUI_EVENT_CLOSE
				GUIDelete($PortableGUI)
				ExitLoop
			Case $FixedButton
				IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "PortableMode", "NO")
				GUIDelete($PortableGUI)
				ExitLoop
			Case $PortableButton
				IniWrite(@ScriptDir & "\config.ini", "KeePassLauncherSettings", "PortableMode", "YES")
				GUIDelete($PortableGUI)
				ExitLoop
		EndSwitch
	WEnd
EndFunc
