#NoTrayIcon
#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=iconlarge.ico
#AutoIt3Wrapper_Outfile=KeePass Launcher.exe
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Description=KeePass Launcher
#AutoIt3Wrapper_Res_Fileversion=3.1.3
#AutoIt3Wrapper_Res_LegalCopyright=(C) TheChiefMeat 2016-2019. All rights reserved.
#AutoIt3Wrapper_Res_HiDpi=y
#AutoIt3Wrapper_Res_Field=ProductName|KeePass Launcher
#AutoIt3Wrapper_Run_Tidy=y
#Au3Stripper_Parameters=/so /rm
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;The #NoTrayIcon line stops the icon appearing in the bottom right of the windows tray.
#include <Decrypt.au3>
#include <Encrypt.au3>
#include <FileLocation.au3>
#include <MiscFunctions.au3>
#include <StringEncrypt.au3>

;==========================================GUI CREATION AND SETTINGS=========================================
#include <GUIConstantsEx.au3>
;!Highly recommended for improved overall performance and responsiveness of the GUI effects etc.! (after compiling):
;Required if you want High DPI scaling enabled. (Also requries _Metro_EnableHighDPIScaling())
#include "MetroGUI-UDF\MetroGUI_UDF.au3"
;Set Theme
_SetTheme("DarkPurple") ;See MetroThemes.au3 for selectable themes or to add more
;Enable high DPI support: Detects the users DPI settings and resizes GUI and all controls to look perfectly sharp.
_Metro_EnableHighDPIScaling() ; Note: Requries "#AutoIt3Wrapper_Res_HiDpi=y" for compiling. To see visible changes without compiling, you have to disable dpi scaling in compatibility settings of Autoit3.exe
;=============================================================================================================

;INCLUDES - These are required libraries for the script to function.

#include <Crypt.au3>
#include <MsgBoxConstants.au3>
#include <FileConstants.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <Constants.au3>
#include <Array.au3>
#include <File.au3>
#include <StaticConstants.au3>

;GLOBAL VARIABLES - These variables are read from the .ini file (if it exists).
$MASTER_KEY_Location = IniRead("config.ini", "EncryptionFileSettings", "KeePassUsrDataLocation", "")
$EXE_Location = IniRead("config.ini", "KeePassLauncherSettings", "ApplicationInstallLocation", "")
$StartMinimised = IniRead("config.ini", "KeePassLauncherSettings", "StartMinimised", "")
$KDBX_Location = IniRead("config.ini", "KeePassLauncherSettings", "DatabaseLocation", "")
$PortableMode = IniRead("config.ini", "KeePassLauncherSettings", "PortableMode", "")
$KEYFILE_Location = IniRead("config.ini", "KeyfileSettings", "KEYFILELocation", "")
$PauseTime = IniRead("config.ini", "KeePassLauncherSettings", "PauseTime", "")
$VERSION_KEEPASS = IniRead("config.ini", "Startup", "VERSION", "")
$VERSION_Launcher = "3.1.3"

;The script here looks for the config.ini file. If it doesn't exist it creates it and stores a Startup variable to let
;you know it's been run at least once, and creates a shortcut on the desktop for ease of use.
If FileExists("config.ini") Then
Else
	FileOpen(@ScriptDir & "\config.ini", 1)
	IniWrite(@ScriptDir & "\config.ini", "Startup", "Startup", "0")
	Local Const $sFilePath = @DesktopDir & "\KeePass Launcher.lnk"
	FileCreateShortcut(@ScriptDir & "\KeePass Launcher.exe", $sFilePath, @ScriptDir, "", "KeePass Launcher", @ScriptDir & "\KeePass Launcher.exe", "", "")
EndIf
;Here the stored Startup variable is read from the config.ini file that was made before.
$Startup = IniRead("config.ini", "Startup", "Startup", "")
;Checking if the encrpyted master key (.xml file) exists. If it does, then the encryption function is run, if not the script continues.
If FileExists($MASTER_KEY_Location) Then
	Decrypt()
	;Checking to see if the Startup variable is 0 or 1. If it equals 1 then the encryption function is run,
	;if it equals 0 then the startup FileLocation function is run.
ElseIf $Startup = "1" Then
	Encrypt()
Else
	FileLocation()
EndIf
